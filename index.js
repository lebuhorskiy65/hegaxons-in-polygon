/**
 @author Vitalii Lebukhorskyi
 @since 04.09.2022
 @class GetHexForPolygon
 */

const collect = require('@turf/turf');
const fs = require('fs')
const data = require('./data.json')

class GetHexForPolygon {

  polygon = null
  sideSize = null

  /**
   * @param polygon array
   * @param sideSize number
   */
  constructor (polygon, sideSize) {
    this.sideSize = sideSize
    this.polygon = this.reverseCoordinates(polygon)
  }

  /**
   * @param coordinates
   * @return []
   */
  reverseCoordinates (coordinates) {
    return coordinates.map((item) => {
      return [item[1], item[0]]
    })
  }

  /**
   * @return []
   */
  get () {
    const line = collect.lineString(this.polygon);
    line.geometry.coordinates.map(i => i.reverse())

    const bbox = collect.bbox(line);
    const polygon = collect.polygon([line.geometry.coordinates])
    const options = {units: 'meters', mask: polygon};

    const grid = collect.hexGrid(bbox, this.sideSize, options);

    // for preview on site https://geojson.io/
    fs.writeFile('result.json', JSON.stringify(grid), () => {})

    const result = [];

    collect.featureEach(grid, (currentFeature) => {
      const coords = currentFeature.geometry.coordinates[0]
      result.push({
        pos0: {
          lat: coords[0][1],
          long: coords[0][0],
        }, // equal last
        pos1: {
          lat: coords[1][1],
          long: coords[1][0]
        },
        pos2: {
          lat: coords[2][1],
          long: coords[2][0]
        },
        pos3: {
          lat: coords[3][1],
          long: coords[3][0]
        },
        pos4: {
          lat: coords[4][1],
          long: coords[4][0]
        },
        pos5: {
          lat: coords[5][1],
          long: coords[5][0]
        },
        pos6: {
          lat: coords[6][1],
          long: coords[6][0]
        }, // equal first
      })
    })

    return result;
  }
}

// call
const service = new GetHexForPolygon(data, 500)
const result = service.get()
console.log(result)
